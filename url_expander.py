from flask import Flask, jsonify
from flask_cors import CORS
from flask import request
from urllib.parse import urlparse
import requests

app = Flask(__name__)
cors = CORS(app, resources={r"/api/*": {"origins": "*"}})


@app.route('/api/expand', methods=['PUT'])
def expand_url():
    try:
        url = request.get_json().get('url')
        url_parsed = urlparse(url)
    except:
        return jsonify({'status': 1, 'url': ''}), 403
    head_urls = ['vm.tiktok.com', 'soundcloud.app.goo.gl']
    get_urls = ['youtube.com']
    if url_parsed.netloc in head_urls:
        r = requests.head(url, timeout=5)
        if r.status_code in [301, 302]:
            return jsonify({'status': 0, 'url':  r.headers['Location']})
    elif url_parsed.netloc in get_urls:
        r = requests.get(url, timeout=5)
        if r.status_code == 200:
            return jsonify({'status': 0, 'url': r.url})
    return jsonify({'status': 1, 'url': ''}), 403


if __name__ == '__main__':
    app.run(debug=True)
